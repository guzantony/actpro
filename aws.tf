provider "aws" {
  region     = "eu-central-1"
  
}

resource "aws_vpc" "actpro-vpc" {
     cidr_block = "10.0.0.0/16"
      tags = {
        Name = "Actpro-net"
  }
}

resource "aws_subnet" "front-end-net" {
  vpc_id     = aws_vpc.actpro-vpc.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "Public-net"
  }
}

resource "aws_subnet" "back-end-net" {
  vpc_id     = aws_vpc.actpro-vpc.id
  cidr_block = "10.0.2.0/24"

  tags = {
    Name = "Private-net"
  }
}
resource "aws_internet_gateway" "Actpro-GW" {
  vpc_id = aws_vpc.actpro-vpc.id

  tags = {
    Name = "Actpro-GW"
  }
}

resource "aws_route_table" "Actpro-RT" {
  vpc_id = aws_vpc.actpro-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.Actpro-GW.id
  } 

  tags = {
    Name = "Actpro-RT-Front"
  }
}

resource "aws_eip" "front" {
  #instance = aws_instance.web-server.id
  vpc      = true
}

resource "aws_nat_gateway" "Actpro-NAT-GW" {
  allocation_id = aws_eip.front.id
  subnet_id     = aws_subnet.front-end-net.id

  tags = {
    Name = "Actpro-NAT-GW"
  }
  depends_on = [aws_internet_gateway.Actpro-GW]
}

resource "aws_route_table" "Actpro-NAT-RT" {
  vpc_id = aws_vpc.actpro-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.Actpro-NAT-GW.id
  } 

  tags = {
    Name = "Actpro-NAT-GW"
  }
}
resource "aws_route_table_association" "a-front-net" {
  subnet_id      = aws_subnet.front-end-net.id
  route_table_id = aws_route_table.Actpro-RT.id
}
resource "aws_route_table_association" "a-back-net" {
  subnet_id      = aws_subnet.back-end-net.id
  route_table_id = aws_route_table.Actpro-NAT-RT.id
}
resource "aws_security_group" "actpro-sg" {
  name        = "ssh-web"
  description = "Allow 22 and 80 ports traffic"
  vpc_id      = aws_vpc.actpro-vpc.id

  ingress {
    description      = "SSH from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "WEB from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "ssh-web-sg"
  }
}

resource "aws_security_group" "actpro-sg-db" {
  name        = "ssh-db"
  description = "Allow 22 port traffic"
  vpc_id      = aws_vpc.actpro-vpc.id

  ingress {
    description      = "SSH from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["10.0.1.0/24"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "ssh-db-sg"
  }
}

data "aws_ami" "ubuntu-latest" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical 
}

resource "aws_instance" "web-server" {
  ami           = data.aws_ami.ubuntu-latest.id
  instance_type = "t2.micro"
  subnet_id = aws_subnet.front-end-net.id
  vpc_security_group_ids = [aws_security_group.actpro-sg.id]
  associate_public_ip_address = true

  key_name = "aws1"
  

  tags = {
    Name = "web-server"
  }
}

output "ec2_public_ip" {
  value = aws_instance.web-server.public_ip
}

resource "aws_instance" "db-server" {
  ami           = data.aws_ami.ubuntu-latest.id
  instance_type = "t2.micro"
  subnet_id = aws_subnet.back-end-net.id
  vpc_security_group_ids = [aws_security_group.actpro-sg-db.id]
  associate_public_ip_address = false
  
  key_name = "aws1"
  

  tags = {
    Name = "db-server"
  }
}